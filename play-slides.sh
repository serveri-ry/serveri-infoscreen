#!/bin/bash

# previous process id
PREVPID=""
# Load system specific variables from .env file
source /opt/infoscreen/.env

function CheckNetwork () {
    FAIL_COUNTER=0
    while ! curl --silent 1.1.1.1 > /dev/null
    do  
        log "Cannot curl webdav url, restarting interface for $FAIL_COUNTER:th time"
        ip link set wlan0 down
        ip link set wlan0 up
        log "interface restarted, checking connection"
        #sleep 10
        # commented sleep because SetMessage takes time
        SetMessage "Waiting for network, reboot if stuck too long (10 minutes)"
        log "Current IP addresses are: $(ip -br addr show | awk '{print $3}' | tr "\n" " ")"

        if [ "$FAIL_COUNTER" -gt "10" ]
        then
            reboot
            FAIL_COUNTER=0
        fi
        let FAIL_COUNTER++
    done
    log "Network connection successiful"
    sleep 2
}

function PreventCpuOverheat () {
    while [ "$(</sys/class/thermal/thermal_zone*/temp)" -gt "63000" ]
    do 
        log "CPUTEMP is $(($(</sys/class/thermal/thermal_zone*/temp)/1000)) °C"
        # 85 C is the temperature limit on Pine64 ROCK64
        SetMessage "Controller CPU is too hot, waiting until it cools down below 63 °C - it is currently at $(($(</sys/class/thermal/thermal_zone*/temp)/1000)) °C. Temperature is refreshed every $IMG_VIEW_TIME seconds."
    done
}

# Kill previous viewer, set new one as previous
function ClosePrevious () {
    kill $PREVPID
    PREVPID=$1
}

function log () {
    echo "$(date +%s) $1" >> /var/log/inforscreen.log
}

function SetMessage () {
    log "generating infoscreen message for $1"
    convert -size 1920x1080 xc:black +repage \
    -size 1100x800  -fill white -background None  \
    -font CourierNewB -gravity center caption:"$1" +repage \
    -gravity Center  -composite -strip /tmp/messagefile.jpeg

    log "image supposedly generated, viewing now"

    /usr/bin/feh --auto-zoom --fullscreen --quiet --slideshow-delay $IMG_VIEW_TIME --on-last-slide=quit /tmp/messagefile.jpeg
    log "exited image viewer"
    #ClosePrevious $!
}

# takes filename as argument, returns extension
function GetExtension () {
    # Match everything except dot from dot to end of line, assumes extension is the last thing on a row
    REGEX="\.[^.]+$"
    echo "$(echo $1 | grep --ignore-case --only-matching --perl-regexp $REGEX | tr '[:upper:]' '[:lower:]')"
}

function CheckAndPlayVideo () {
    FILE_HEIGHT=$(exiftool $(echo "$MEDIAROOT/$1") | grep --perl-regex "^Image Height" | awk '{print $4}')
    if [ "$FILE_HEIGHT" -le "$MAX_FRAME_HEIGHT" ]
    then
        /usr/bin/mpv -fs -vo=gpu $(echo "$MEDIAROOT/$1")
        echo "video $1 can be played"
    else
        log "File $1 is $FILE_HEIGHT pixels tall, however limit is set to $MAX_FRAME_HEIGHT"
        echo "File $1 is too large"
    fi
}

# takes filename as argument
function PlayMedia () {
    EXTENSION=$(GetExtension $1)
    case $EXTENSION in
        .avi)
            echo "$1 is avi"
        ;;
        .png | .jpeg | .jpg)
            echo "$1 is png"
            /usr/bin/feh --auto-zoom --fullscreen --quiet --slideshow-delay $IMG_VIEW_TIME --on-last-slide=quit $(echo "$MEDIAROOT/$1")
            #ClosePrevious $!
        ;;
        .mov | .mp4)
            CheckAndPlayVideo $1
            #ClosePrevious $!
        ;;
    esac
}

function playback () {
    while $(cat /opt/infoscreen/continue-playing)
    do
        # Listing will define the ordering of content media files
        DIR_ITEMS=$(ls -1 --sort=version $MEDIAROOT)
        echo "$DIR_ITEMS" | while IFS= read -r ITEM
        do  
            PreventCpuOverheat
            # Chech file exists and is file
            if [ -f $(echo "$MEDIAROOT/$ITEM") ]
            then
                PlayMedia "$ITEM"
            fi
        done
        #sleep 10
        CheckNetwork
    done
}

CheckNetwork

if [ "$DAVENABLED" == "true" ]
then
    log "Opening up connection to remote media source, Current IP addresses are: $(ip -br addr show | awk '{print $3}' | tr "\n" " ")"
    # if [ $(ls /opt/infoscreen/images) ]
    # then
        wall "Mounting webdav volume"
        echo "Mounting webdav volume"
        printf "cache_size       $WEBDAV_MAX_CACHESIZE\ndir_refresh     $WEBDAV_DIR_REFRESH\nfile_refresh    $WEBDAV_FILE_REFRESH\n" > /etc/davfs2/davfs2.conf
        # try to unmount before mounting
        umount $MEDIAROOT
        sleep 2
        expect /opt/infoscreen/mountdav.exp $DAVURL $DAVUSER $DAVPASSWD $MEDIAROOT
    # fi
fi

wall "Starting infoscreen"
# Call the functions
echo "Beginning playback"
playback

# feh --fullscreen --auto-zoom --quiet --recursive --slideshow-delay 10 --sort name --hide-pointer ../images 
# mplayer -fs DJI_0688.MP4
# this works:
# startx /usr/bin/mpv -vo=x11 -ao=null /root/Downloads/file_example_MP4_1920_18MG.mp4
# startx /usr/bin/mpv -vo=gpu -loop=inf /root/Downloads/file_example_MP4_1920_18MG.mp4