#!/bin/bash
touch /var/log/inforscreen.log

# Workaround to let the system properly boot before attempting filesystem mounts
wall "Current IP addresses are: $(ip -br addr show | awk '{print $3}' | tr "\n" " ")"
sleep 15

startx /opt/infoscreen/play-slides.sh -- -nocursor #/usr/bin/mpv -vo=gpu -loop=inf 